package types

import (
	"context"

	calendar "google.golang.org/api/calendar/v3"
)

type Contact struct {
	ClientAdresse       string `json:"client_adresse"`
	ClientMail          string `json:"client_mail"`
	ClientNom           string `json:"client_nom"`
	ClientPrenom        string `json:"client_prenom"`
	ClientTelephone     string `json:"client_telephone"`
	ContactPreference   string `json:"contact_preference"`
	GiteInitial         string `json:"gite_initial"`
	GiteSelected        string `json:"gite_selected"`
	Lieu                string `json:"lieu"`
	MailMessage         string `json:"mail_message"`
	MailSujet           string `json:"mail_sujet"`
	MontantArrhes       int    `json:"montant_arrhes"`
	MontantSejour       int    `json:"montant_sejour"`
	NombreNuits         int    `json:"nombre_nuits"`
	NombrePersonnes     int    `json:"nombre_personnes"`
	NombrePersonnesSupp int    `json:"nombre_personnes_supp"`
	TarifNuit           int    `json:"tarif_nuit"`
	PayBefore           string `json:"pay_before"`
	SejourDebut         string `json:"sejour_debut"`
	SejourFin           string `json:"sejour_fin"`
	Progress            string `json:"progress"`
	ID                  string `json:"ID"`
	Alternative         string `json:"alternative"`
	GiteURL             string `json:"gite_url"`
}

type GiteCalendar struct {
	Initial    string
	GiteName   string
	CalendarID string
	ColorID    string
}

var (
	Calendar map[string]GiteCalendar
	Cal      *calendar.Service
	err      error
	ctx      context.Context = context.Background()
)

func makeCalendar() {
	Calendar = make(map[string]GiteCalendar)
	Calendar["to"] = GiteCalendar{
		ColorID:    "5",
		Initial:    "to",
		GiteName:   "La Topaze d'Or",
		CalendarID: "ed9bbb7add411bd822fd025b3e69b9a145d6457214fd899ef3ce0e753736f3fb@group.calendar.google.com",
	}
	Calendar["js"] = GiteCalendar{
		ColorID:    "7",
		Initial:    "js",
		GiteName:   "Le Joyeux Saphir",
		CalendarID: "e1a4df2c36cd038a641ab3829cd36f55444720e6093b69fc4bdd84157137b224@group.calendar.google.com",
	}
	Calendar["ra"] = GiteCalendar{
		ColorID:    "4",
		Initial:    "ra",
		GiteName:   "Le Rubis Ardent",
		CalendarID: "85b30747263791c986c4b03e2300bdfb2bb42bbb1d5ac307415fc2ac80dac5fe@group.calendar.google.com",
	}
	Calendar["be"] = GiteCalendar{
		ColorID:    "2",
		Initial:    "be",
		GiteName:   "La Belle Emeraude",
		CalendarID: "227213d5d4130aafddd556aa503ede331a6b0c74745a19f84e96875bf6c7806d@group.calendar.google.com",
	}
	Calendar["jp"] = GiteCalendar{
		ColorID:    "3",
		Initial:    "jp",
		GiteName:   "Le Jupiter",
		CalendarID: "9fd346dbdf2a4e66e8728cabc937fa53f9163347b205c2a667c242cf6c5e9c0a@group.calendar.google.com",
	}
}
