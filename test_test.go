package types

import (
	"encoding/base64"
	"encoding/json"

	gdrive "google.golang.org/api/drive/v3"
	"log"
	"os"
	"testing"

	"google.golang.org/api/option"
)

func TestSHA1(t *testing.T) {
	c := Contact{}
	c.GenerateID()
	log.Println(c.ID)
}

func TestListAllEvent(t *testing.T) {
	c := Contact{}
	js, _ := os.ReadFile("test.json")
	json.Unmarshal(js, &c)

	c.ID = "d561461d6119"
	_, pre, _ := c.EventPresent()

	log.Println(pre)
}

func TestCreateEvent(t *testing.T) {
	c := Contact{}
	js, _ := os.ReadFile("test.json")
	json.Unmarshal(js, &c)

	c.ID = "e3e987f6c24d"
	c.CalCreateEvent()
}

func TestCreateTemplate(t *testing.T) {
	c := Contact{}
	js, _ := os.ReadFile("test.json")
	json.Unmarshal(js, &c)

	log.Println(c.calEventDescription())
}

func TestDriveGoogle(t *testing.T) {
	auth, _ := base64.StdEncoding.DecodeString(os.Getenv("SERVICE_GOOGLE_AUTH"))
	driv, err := gdrive.NewService(ctx, option.WithCredentialsJSON(auth))
	if err != nil {
		log.Fatalln(err)
	}

	f, err := os.Open("test.json")
	if err != nil {
		log.Fatalf("error opening %q: %v", "test.json", err)
	}
	defer f.Close()
	driveFile, err := driv.Files.Create(&gdrive.File{
		Name:    "test.json",
		Parents: []string{"1HyQE13nMmf7845kej7vdxn-Qse5SINcl"}}).Media(f).Do()
	if err != nil {
		log.Fatalf("Unable to create file: %v", err)
	}
	log.Printf("file: %+v", driveFile)

	app := driv.Apps.List()
	log.Println(app)
}
