package types

import (
	"bytes"
	"encoding/base64"
	"encoding/json"
	"log"
	"os"
	"strings"
	"text/template"
	"time"

	calendar "google.golang.org/api/calendar/v3"
	"google.golang.org/api/option"
)

func (co *Contact) CalCreateEvent() error {
	makeCalendar()
	auth, _ := base64.StdEncoding.DecodeString(os.Getenv("SERVICE_GOOGLE_AUTH"))
	Cal, err = calendar.NewService(ctx, option.WithCredentialsJSON(auth))
	if err != nil {
		log.Fatalln(err)
	}

	summary := ""
	if co.Progress != "" {
		summary = strings.ToUpper(co.GiteInitial) + " | " + co.ClientNom + " " + co.ClientPrenom + ". Payé: " + co.Progress + "."
	} else {
		summary = strings.ToUpper(co.GiteInitial) + " | " + co.ClientNom + " " + co.ClientPrenom + "."
	}

	private := make(map[string]string)
	private["ID"] = co.ID
	private["progress"] = co.Progress

	ddebut, _ := time.Parse("2/1/2006", co.SejourDebut)
	dfin, _ := time.Parse("2/1/2006", co.SejourFin)

	colorID := Calendar[co.GiteInitial].ColorID
	if co.Progress == "aucun" {
		colorID = "8"
	}

	calEvent := &calendar.Event{
		Summary:     summary,
		Description: co.calEventDescription(),
		Start: &calendar.EventDateTime{
			DateTime: string(ddebut.Add(10 * time.Hour).Format(time.RFC3339)),
		},
		End: &calendar.EventDateTime{
			DateTime: string(dfin.Add(10 * time.Hour).Format(time.RFC3339)),
		},
		Status:       "tentative",
		Transparency: "transparent",
		ColorId:      colorID,
		ExtendedProperties: &calendar.EventExtendedProperties{
			Private: private,
		},
	}

	ev, present, _ := co.EventPresent()

	if present {
		_, err := Cal.Events.Update(Calendar[co.GiteInitial].CalendarID, ev.Id, calEvent).SendUpdates("all").Do()
		if err != nil {
			return err
		}
	} else {
		_, err := Cal.Events.Insert(Calendar[co.GiteInitial].CalendarID, calEvent).SendUpdates("all").Do()
		if err != nil {
			return err
		}
	}

	return nil
}

func (co *Contact) EventPresent() (calEvent *calendar.Event, present bool, erro error) {
	events, err := Cal.Events.List(Calendar[co.GiteInitial].CalendarID).Do()
	if err != nil {
		return &calendar.Event{}, false, err
	}

	for _, en := range events.Items {
		testPrivate := map[string]map[string]string{}
		testPrivateKey := en.ExtendedProperties

		j, _ := json.Marshal(testPrivateKey)
		json.Unmarshal(j, &testPrivate)

		if _, ok := testPrivate["private"]["ID"]; ok && testPrivate["private"]["ID"] == co.ID && testPrivate["private"]["ID"] != "" {
			return en, true, nil
		}
	}

	for key, configCal := range Calendar {
		if key == co.GiteInitial {
			continue
		}

		events, err := Cal.Events.List(configCal.CalendarID).Do()
		if err != nil {
			return &calendar.Event{}, false, err
		}

		for _, en := range events.Items {
			testPrivate := map[string]map[string]string{}
			testPrivateKey := en.ExtendedProperties

			j, _ := json.Marshal(testPrivateKey)
			json.Unmarshal(j, &testPrivate)

			if _, ok := testPrivate["private"]["ID"]; ok && testPrivate["private"]["ID"] == co.ID && testPrivate["private"]["ID"] != "" {
				return en, true, nil
			}
		}

	}

	return &calendar.Event{}, false, nil
}

func (co *Contact) CalDeleteEvent() error {
	makeCalendar()
	auth, _ := base64.StdEncoding.DecodeString(os.Getenv("SERVICE_GOOGLE_AUTH"))
	Cal, err = calendar.NewService(ctx, option.WithCredentialsJSON(auth))
	if err != nil {
		log.Fatalln(err)
	}

	ev, present, _ := co.EventPresent()

	if present {
		Cal.Events.Delete(Calendar[co.GiteInitial].CalendarID, ev.Id).SendUpdates("all").Do()
	}

	return nil
}

func (co *Contact) calEventDescription() string {
	var buf bytes.Buffer
	var descrption = `Nom du client: {{ .ClientNom }} {{ .ClientPrenom }}
Début: {{ .SejourDebut }}
Fin: {{ .SejourFin }}
Nombre de nuits: {{ .NombreNuits }}
Nombre de personnes: {{ .NombrePersonnes }}
Tarif séjour: {{ .MontantSejour }} €
Paiement: {{ .Progress }}
	`

	tmpl, _ := template.New("").Parse(descrption)
	_ = tmpl.Execute(&buf, co)

	return buf.String()
}
