package types

import (
	"crypto/sha1"
	"encoding/hex"
	"encoding/json"
	"strings"
)

// Generate ID
func (c *Contact) GenerateID() {
	if c.ID != "" {
		return
	}

	dataclient, _ := json.Marshal(c)
	h := sha1.New()
	h.Write(dataclient)
	c.ID = hex.EncodeToString(h.Sum(nil))[:12]
}

// RevertDateDebut renvoie la date de début au format YYYY/mm/dd
func (c *Contact) RevertDateDebut() string {
	d := strings.Split(c.SejourDebut, "/")
	return d[2] + "/" + d[1] + "/" + d[0]
}

// RevertDateFin renvoie la date de fin au format YYYY/mm/dd
func (c *Contact) RevertDateFin() string {
	d := strings.Split(c.SejourDebut, "/")
	return d[2] + "/" + d[1] + "/" + d[0]
}

// MailSansPoint remplace les "." par des "_"
func (c *Contact) MailSansPoint() string {
	m := strings.ReplaceAll(c.ClientMail, ".", "_")
	return m
}

// Supprimer un contact suivant sa date de début du séjour
func (c *Contact) RemoveContact(allContact map[string]map[string]map[string]Contact) map[string]map[string]map[string]Contact {
	delete(allContact[c.GiteInitial][c.MailSansPoint()], c.RevertDateDebut())

	if len(allContact[c.GiteInitial][c.MailSansPoint()]) == 0 {
		delete(allContact[c.GiteInitial], c.MailSansPoint())
	}

	return allContact
}

// Ajouter un contact
func (c *Contact) UpdateContact(allContact map[string]map[string]map[string]Contact) map[string]map[string]map[string]Contact {
	if c.GiteInitial == "" {
		return allContact
	}

	if _, ok := allContact[c.GiteInitial]; !ok {
		allContact[c.GiteInitial] = make(map[string]map[string]Contact)
	}

	if _, ok := allContact[c.GiteInitial][c.MailSansPoint()]; !ok {
		allContact[c.GiteInitial][c.MailSansPoint()] = make(map[string]Contact)
	}

	allContact[c.GiteInitial][c.MailSansPoint()][c.RevertDateDebut()] = *c

	return allContact
}

// Change l'avancement du process {"aucun", "arrhes", "finalise"}
func (c *Contact) SetProgress(progress string) {
	var processList []string = []string{"aucun", "arrhes", "finalise"}
	for _, pr := range processList {
		if pr == progress {
			c.Progress = progress
			break
		}
	}
}
